#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#include "get_next_line.h"

char *ft_separate(char **store, int pos)
{
	char *res;
	char *hold_st;
	char *tmp;
	int i;

	hold_st = *store;
	if (!(res = (char *)malloc(sizeof(char) * (pos + 1))))
		return (NULL);
	i = 0;
	while (i < pos && *hold_st)
		*(res + i++) = *hold_st++;
	*(res + i) = '\0';
	if (!(tmp = (char *)malloc(sizeof(char) * (ft_strlen(hold_st)))))
		return (NULL);
	i = 0;
	while (*hold_st++)
		*(tmp + i++) = *hold_st;
	*(tmp + i) = '\0';
	free(*store);
	*store = NULL;
	*store = tmp;
	return (res);
}

char *ft_strjoin(char *store, char *buff)
{
	char *res;
	char *tmp;
	int len;
	int ls;

	ls = ft_strlen(store);
	len = ls + ft_strlen(buff);
	if (!(res = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	tmp = res;
	while (store && *store)
		*tmp++ = *store++;
	while (buff && *buff)
		*tmp++ = *buff++;
	*tmp = '\0';
	free(store - ls);
	return (res);
}

int term(char *buff, char **store, int ret)
{
	if (buff)
		free(buff);
	if (store)
		*store = NULL;
	return (ret);
}

int init_err(int fd, char **line, char **buff)
{
	if (fd < 0 || BUFFER_SIZE <= 0 || !line)
		return (ERR_);
	if (!(*buff = (char *)malloc(sizeof(char) * (BUFFER_SIZE + 1))))
		return (ERR_);
	if (read(fd, *buff, 0) == -1)
	{
		free(*buff);
		return (ERR_);
	}
	return (0);
}

int get_next_line(int fd, char **line)
{
	char *buff;
	static char *store;
	int pos;
	int n;

	if (init_err(fd, line, &buff))
		return (ERR_);
	pos = -1;
	while (1)
	{
		while (((pos = ft_charpos(store, CHAR_NEW)) == -1) &&
			   (n = read(fd, buff, BUFFER_SIZE)))
		{
			buff[n] = '\0';
			if (!(store = ft_strjoin(store, buff)))
				return (term(buff, NULL, ERR_));
		}
		if ((pos == -1) && (n == 0))
			break;
		*line = ft_separate(&store, pos);
		free(buff);
		return (!store ? ERR_ : SUC_);
	}
	*line = ft_strjoin(store, NULL);
	return ((!*line) ? term(buff, &store, ERR_) : term(buff, &store, EOF_));
}
