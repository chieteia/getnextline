
#ifndef GET_NEXT_LINE_H
#define GET_NEXT_LINE_H

#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#define CHAR_NEW '\n'

int ft_strlen(char *str);
int ft_charpos(char *str, char c);
char *ft_separate(char **store, int pos);
char *ft_strjoin(char *store, char *buff);
int term(char *buff, char **store, int ret);
int get_next_line(int fd, char **line);

typedef enum
{
	ERR_ = -1,
	EOF_,
	SUC_,
} t_result;

#endif
